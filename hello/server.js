#!/usr/bin/env node --harmony
'use strict'

const
  express = require('express')
, redisClient = require('redis').createClient()
, RedisStore = require('connect-redis')(express)
, app = express()

app
  .use(express.logger('dev'))
  .use(express.cookieParser())
  .use(express.session(
    { secret : 'keyboard-cat'
    , store  : new RedisStore({ client: redisClient })
    }
  )
)

app.get('/api/:name', function(req, res) {
  res.json(200, { "Hello": req.params.name })
})

app.listen(3000, function(){
  console.log("Hello is ready...")
})

